var game = new Phaser.Game(832, 256, Phaser.AUTO, 'phaser-example', {preload: preload, create: create, update: update });
var bigletters = [];
var littleletters = [];
var bigsprites = [];
var littlesprites = [];
var alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
var bigorder = [];
var littleorder = [];
var selected = [];
var background;
function preload(){
    background = game.load.image('background', 'images/back.png');
    for(let i = 0; i < 26; i++){
        let rnd = random(0, alphabet.length);
        let letter = alphabet[rnd];
        bigorder.push(letter);
        alphabet.splice(rnd, 1);
        bigletters.push(game.load.image('letter' + letter, 'images/big' + letter.toLowerCase() + '.png'));
        
    }
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    for(let i = 0; i < 26; i++){
        let rnd = random(0, alphabet.length);
        let letter = alphabet[rnd];
        littleorder.push(letter.toLowerCase());
        alphabet.splice(rnd, 1);
        littleletters.push(game.load.image('letter' + letter.toLowerCase(), 'images/little' + letter.toLowerCase() + '.png'));
    }
    
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
}
function create(){
    game.add.image(0,0,'background');
    for(let i = 0; i < 26; i++){
        if (i < 13){
        let sprite = game.add.sprite(i*64, 0, 'letter' + bigorder[i]);
        sprite.scale.setTo(2,2);
        sprite.letter = bigorder[i];
        sprite.matched = false;
        sprite.inputEnabled = true;
        sprite.events.onInputDown.add(listener, this);
        bigsprites.push(sprite);
        
        let lsprite = game.add.sprite(i*64, 128, 'letter' + littleorder[i]);
        lsprite.scale.setTo(2,2);
        lsprite.letter = littleorder[i];
        lsprite.matched = false;
        lsprite.inputEnabled = true;
        lsprite.events.onInputDown.add(listener, this);
        littlesprites.push(sprite);
        } else {
            let sprite = game.add.sprite(i*64 - 832, 64, 'letter' + bigorder[i]);
        sprite.letter = bigorder[i];
        sprite.scale.setTo(2,2);
        sprite.matched = false;
        sprite.inputEnabled = true;
        sprite.events.onInputDown.add(listener, this);
        bigsprites.push(sprite);
        
        let lsprite = game.add.sprite(i*64 - 832, 192, 'letter' + littleorder[i]);
        lsprite.letter = littleorder[i];
        lsprite.scale.setTo(2,2);
        lsprite.matched = false;
        lsprite.inputEnabled = true;
        lsprite.events.onInputDown.add(listener, this);
        littlesprites.push(sprite);
        }
    }
    
}
function update(){
    let alive = false;
    for(let i = 0; i < 26; i++){
        if (bigsprites[i].alive){
            alive = true;
        }
    }
    if (!alive){
        let style = { font: "32px Arial", fill: "#ffffff", align: "center"};
        //game.add.text(0,0, 'YOU WIN!!!!1!!1!!', style);
    }
}
function random(min, max){
    return Math.floor(Math.random() * (max - min) + min);
}

function listener(sprite, point){
    
    if (selected && selected.length === 1){
        compare(selected[0], sprite);
        selected = [];
    } else {
        selected.push(sprite);
    }
    
}

function compare(a, b){
    
    if (a.letter.toLowerCase() === b.letter || b.letter.toLowerCase() === a.letter){
        a.destroy();
        b.destroy();
        
    }
}